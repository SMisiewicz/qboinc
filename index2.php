<?php
   include_once("gui_rpc_client.php");
   include_once("utils.php");
   include_once("config.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <link rel="stylesheet" type="text/css" href="default.css">
  </head>

  <body>
    <?php

            $client = new RPC_CLIENT();
            $address = "localhost";
            if ($client->connect($address, 31416)) {
                $client_state = $client->get_state();
                //print_r($client_state["json"]);
                print_work_json($client, $client_state, $address);
            } else {
                echo "offline";
            }  
           $client->close();
           
        ?>
  </body>
</html>
