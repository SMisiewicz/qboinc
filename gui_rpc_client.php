<?php
//
// $Id: gui_rpc_client.php,v 1.5 2005/04/16 01:25:27 charlie Exp $
//
// $Log: gui_rpc_client.php,v $
// Revision 1.5  2005/04/16 01:25:27  charlie
// clean up
//
// Revision 1.4  2005/04/16 00:37:15  charlie
// core client version added to header and footer
//
//

define("DATE",     "Y-m-d H:m:s");
include_once("parse.php");
include_once("gui_passwd.php");

class RPC_CLIENT {
    var $socket;

    function RPC_CLIENT() {
       $this->socket = socket_create(AF_INET, SOCK_STREAM, 0);
    }

    function connect($address="localhost", $port=31416) {
        return socket_connect($this->socket, $address, $port);
    }

    function close() {
        return socket_close($this->socket);
    }

    function send($cmd) {
        $buf = "<boinc_gui_rpc_request>\n".$cmd."\n</boinc_gui_rpc_request>\n\003";
        return socket_write($this->socket, $buf, strlen($buf));
    }

    function read() {
              $data="";
        while (($buf = @socket_read($this->socket, 4096)) !== false) {
              $data .= $buf;
              if (preg_match("</boinc_gui_rpc_reply>", $data)) break;
        }
        return $data;
    }

    function get_state() {
        if (!$this->socket) return false;

        $result = $this->send("<get_state/>");
        if (!$result) return false;

        $data = $this->read();

        return $data;
    }

    function get_file_transfers() {
        if (!$this->socket) return false;

        $result = $this->send("<get_file_transfers/>");
        if (!$result) return false;

        $data = $this->read();

        return $this->parse_transfers($data);
    }

// get_version is not a legal rpc command.  However, the
// client always returns its version for any command
// passed to it.  An illegal command just has an error message
// saying its a bad command and not a lot of other stuff we'd
// have to parse.
//    function get_version() {
//        if (!$this->socket) return false;
//
//        $result = $this->send("<get_version/>");
//        //if (!$result) return false;
//
//        $data = $this->read();
//
//        return $this->parse_version($data);
//    }
    function lookup_workunit($workunits, $wu_name) {
        foreach ($workunits as $workunit) {
            if ($workunit["name"]==$wu_name) return $workunit;
        }
        return false;
    }

    function lookup_result($results, $result_name) {
        foreach ($results as $result) {
            if ($result["name"]==$result_name) return $result;
        }
        return false;
    }

//    function parse_version($version)
//    {
//       $strings = explode("\n", $version);
//       $i = -1;
//       $client_version=0;
//       while ($buf = $strings[++$i])
//       {
//          if (parse_int($buf, "client_version", $client_version)) break;
//       }
//       $client_version = $client_version/100;
//       return $client_version;
//    }

    function parse_transfers($transfers) {
        $strings = explode("\n", $transfers);
        $file_xfers = array();
        $i = 0;

        while ($buf = $strings[++$i]) {
            if (match_tag($buf, "<file_transfers>")) continue;
            if (match_tag($buf, "</file_transfers>")) break;
            if (match_tag($buf, "<file_transfer>")) {
                $file_xfer = array("bytes_xferred"=>"0", "file_offset"=>"0", "xfer_speed"=>"0", "hostname"=>"");
                while ($buf = $strings[++$i]) {
                    if (match_tag($buf, "</file_transfer>")) break;
                    if (parse_string($buf, "project_url", $file_xfer["project_url"])) continue;
                    if (parse_string($buf, "name", $file_xfer["name"])) continue;
                    if (parse_double($buf, "nbytes", $file_xfer["nbytes"])) continue;
                    if (parse_double($buf, "max_nbytes", $file_xfer["max_nbytes"])) continue;
                    if (parse_int($buf, "status", $file_info["status"])) continue;
                    if (match_tag($buf, "<generated_locally/>")) $file_xfer["generated_locally"] = true;

                    if (match_tag($buf, "<persistent_file_xfer>")) {
                        while ($buf = $strings[++$i]) {
                            if (match_tag($buf, "</persistent_file_xfer>")) break;
                            if (parse_int($buf, "num_retries", $file_xfer["num_retries"])) continue;
                            if (parse_double($buf, "first_request_time", $file_xfer["first_request_time"])) continue;
                            if (parse_double($buf, "next_request_time", $file_xfer["next_request_time"])) continue;
                            if (parse_double($buf, "time_so_far", $file_xfer["time_so_far"])) continue;
                        }
                    }
                    if (match_tag($buf, "<file_xfer>")) {
                        while ($buf = $strings[++$i]) {
                            if (match_tag($buf, "</file_xfer>")) break;
                            if (parse_double($buf, "bytes_xferred", $file_xfer["bytes_xferred"])) continue;
                            if (parse_double($buf, "file_offset", $file_xfer["file_offset"])) continue;
                            if (parse_double($buf, "xfer_speed", $file_xfer["xfer_speed"])) continue;
                            if (parse_string($buf, "hostname", $file_xfer["hostname"])) continue;
                        }
                        $file_xfer["active"] =true;
                    }
//                    else {
//                        $file_xfer["active"] =false;
//                    }
                }

                array_push($file_xfers, $file_xfer);
            }
        }

        return $file_xfers;
    }

    function run_benchmarks() {

        $result = $this->send("<run_benchmarks/>");
        if (!$result) return false;

        if (($buf = $this->$read()) !== false) {
            return (preg_match("<success/>", $buf));
        }
    }

    function show_graphics() {

        if (!$this->socket) return false;

        $cmd = "<result_show_graphics>\n";
//        $result = @socket_write($this->socket, $cmd, strlen($cmd));
        $result = $this->send($cmd);
        if (!$result) return false;

//         if (($buf = @socket_read($this->socket, 128)) !== false) {
         if (($buf = $this->read()) !== false) {
         return (preg_match("<success/>", $buf));
        }
    }

    function project_update($url) {

        if (!$this->socket) return false;
        if (!$url) return false;

        $cmd =
            "<project_update>\n".
            "<project_url>".$url."</project_url>\n".
            "</project_update>\n";
        $result = $this->send($cmd);
        if (!$result) return false;

        if (($buf = $this->read()) !== false) {
            return (preg_match("<success/>", $buf));
        }
    }

    function project_suspend($url) {

        if (!$this->socket) return false;
        if (!$url) return false;

        if (!$this->authorize()) return false;

        $cmd =
            "<project_suspend>\n".
            "<project_url>".$url."</project_url>\n".
            "</project_suspend>\n";
        $result = $this->send($cmd);
        if (!$result) return false;

        if (($buf = $this->read()) !== false) {
            return (preg_match("<success/>", $buf));
        }
    }

    function project_resume($url) {

        if (!$this->socket) return false;
        if (!$url) return false;

        if (!$this->authorize()) return false;

        $cmd =
            "<project_resume>\n".
            "<project_url>".$url."</project_url>\n".
            "</project_resume>\n";
        $result = $this->send($cmd);
        if (!$result) return false;

        if (($buf = $this->read()) !== false) {
            return (preg_match("<success/>", $buf));
        }
    }

    function project_freeze($url) {

        if (!$this->socket) return false;
        if (!$url) return false;

        if (!$this->authorize()) return false;

        $cmd =
            "<project_nomorework>\n".
            "<project_url>".$url."</project_url>\n".
            "</project_nomorework>\n";
        $result = $this->send($cmd);
        if (!$result) return false;

        if (($buf = $this->read()) !== false) {
            return (preg_match("<success/>", $buf));
        }
    }

    function project_thaw($url) {

        if (!$this->socket) return false;
        if (!$url) return false;
        
        if (!$this->authorize()) return false;
        
        $cmd =
            "<project_allowmorework>\n".
            "<project_url>".$url."</project_url>\n".
            "</project_allowmorework>\n";
        $result = $this->send($cmd);
        if (!$result) return false;

        if (($buf = $this->read()) !== false) {
            return (preg_match("<success/>", $buf));
        }
    }

    function result_suspend($rslt, $url) {

        if (!$this->socket) return false;
        if (!$url) return false;

        if (!$this->authorize()) return false;

        $cmd =
            "<suspend_result>\n".
            "<project_url>".$url."</project_url>\n".
            "<name>".$rslt."</name>\n".
            "</suspend_result>\n";
        $result = $this->send($cmd);
        if (!$result) return false;

        if (($buf = $this->read()) !== false) {
            return (preg_match("<success/>", $buf));
        }
    }

    function result_resume($rslt, $url) {

        if (!$this->socket) return false;
        if (!$url) return false;

        if (!$this->authorize()) return false;

        $cmd =
            "<resume_result>\n".
            "<project_url>".$url."</project_url>\n".
            "<name>".$rslt."</name>\n".
            "</resume_result>\n";
        $result = $this->send($cmd);
        if (!$result) return false;

        if (($buf = $this->read()) !== false) {
            return (preg_match("<success/>", $buf));
        }
    }

    function result_abort($rslt, $url) {

        if (!$this->socket) return false;
        if (!$url) return false;

        if (!$this->authorize()) return false;

        $cmd =
            "<abort_result>\n".
            "<project_url>".$url."</project_url>\n".
            "<name>".$rslt."</name>\n".
            "</abort_result>\n";
        $result = $this->send($cmd);
        if (!$result) return false;

        if (($buf = $this->read()) !== false) {
            return (preg_match("<success/>", $buf));
        }
    }

    function retry_transfer($file, $url) {

        if (!$this->socket) return false;
        if (!$url) return false;

        $cmd =
            "<retry_file_transfer>\n".
            "<project_url>".$url."</project_url>\n".
            "<filename>".$file."</filename\n".
            "</retry_file_transfer>\n";

        $result = $this->send($cmd);
        if (!$result) return false;

        if (($buf = $this->read()) !== false) {
            return (preg_match("<success/>", $buf));
        }
    }


    function project_reset($url) {

        if (!$this->socket) return false;
        if (!$url) return false;

        $cmd =
            "<project_reset>\n".
            "<project_url>".$url."</project_url>\n".
            "</project_reset>\n";
        $result = $this->send($cmd);
        if (!$result) return false;

        if (($buf = $this->read()) !== false) {
            return (preg_match("<success/>", $buf));
        }
    }

    function project_detach($url) {

        if (!$this->socket) return false;
        if (!$url) return false;

        if (!$this->authorize()) return false;

        $cmd =
            "<project_detach>\n".
            "<project_url>".$url."</project_url>\n".
            "</project_detach>\n";
        $result = $this->send($cmd);
        if (!$result) return false;

        if (($buf = $this->read()) !== false) {
            return (preg_match("<success/>", $buf));
        }
    }

    function lookup_account($url, $email, $passwd_hash, &$result) {
        if (!$this->socket) return false;
        if (!$url||!$email||!$passwd_hash) return false;

        if (!$this->authorize()) return false;    

        $split = explode("@", $email);
        if (count($split)!=2) return false;

        $cmd =
            "<lookup_account>\n".
            "<url>".$url."</url>\n".
            "<email_addr>".$split[0]."</email_addr>\n".
            "<passwd_hash>".$passwd_hash."</passwd_hash>\n".
            "<ldap_auth>0</ldap_auth>\n".
            "<server_assigned_cookie>0</server_assigned_cookie>\n".
            "<server_cookie></server_cookie>\n".
            "</lookup_account>\n";
        $result = $this->send($cmd);
        if (!$result) return false;

        if (($buf = $this->read()) !== false) {
            if (!preg_match("<success/>", $buf)) {
                return false;
            }
        }

        $auth = "";
        $max = 10;
        while ($max>0) {
            $cmd =             
                "<lookup_account_poll/>\n";

            $result = $this->send($cmd);
            if (!$result) return false;

            if (($buf = $this->read()) !== false) {
                $error_num = ""; $auth = "";
                if (parse_string($buf, "error_num", $error_num)) {
                    if ($error_num == "-204") {
                        sleep(1);
                        continue;
                    } elseif ($error_num == "-206") { //wrong password
                        $result = $error_num;
                        return true;
                    }

                    $result = $error_num;
                    return false;
                }
                if (parse_string($buf, "authenticator", $auth)) {
                    $result = $auth;
                    return true;
                }
            }
            $max--;
        }

        return false;

    }

   function create_account($url, $email, $passwd_hash, &$result) {
        if (!$this->socket) return false;
        if (!$url||!$email||!$passwd_hash) return false;

        if (!$this->authorize()) return false;    

        $split = explode("@", $email);
        if (count($split)!=2) return false;

        $cmd =
            "<create_account>\n".
            "<url>".$url."</url>\n".
            "<email_addr>".$email."</email_addr>\n".
            "<passwd_hash>".$passwd_hash."</passwd_hash>\n".
            "<user_name>".$split[0]."</user_name>\n".
            "</create_account>\n";        
        $result = $this->send($cmd);
        if (!$result) return false;

        if (($buf = $this->read()) !== false) {
            if (!preg_match("<success/>", $buf)) {
                return false;
            }
        }

        $auth = "";
        $max = 10;
        while ($max>0) {
            $cmd =             
                "<create_account_poll/>\n";

            $result = $this->send($cmd);
            if (!$result) return false;

            if (($buf = $this->read()) !== false) {
                $error_num = ""; $auth = "";
                if (parse_string($buf, "error_num", $error_num)) {
                    $result = $error_num;
                    return false;
                }
                if (parse_string($buf, "authenticator", $auth)) {
                    $result = $auth;
                    return true;
                }
            }
            $max--;
        }

        return false;

   }

    function project_attach($url, $key) {

        if (!$this->socket) return false;
        if (!$url||!$key) return false;

        if (!$this->authorize()) return false;

        $cmd =
            "<project_attach>\n".
            "<project_url>".$url."</project_url>\n".
            "<authenticator>".$key."</authenticator>\n".
            "</project_attach>\n";
        $result = $this->send($cmd);
        if (!$result) return false;

        if (($buf = $this->read()) !== false) {
            return (preg_match("<success/>", $buf));
        }
    }

    function set_run_mode($mode) {

        if (!$this->socket) return false;

        switch ($mode) {
            case "0":
                $cmd = "<always>";
                break;
            case "1":
                $cmd = "<auto>";
                break;
            case "2":
                $cmd = "<never>";
                break;
            default:
                return false;
        }

        $result = $this->send("<set_run_mode>\n".$cmd."\n</set_run_mode>\n");
        if (!$result) return false;

        if (($buf = $this->$read()) !== false) {
            return (preg_match("<success/>", $buf));
        }
    }

    function set_network_mode($mode) {

        if (!$this->socket) return false;

        switch ($mode) {
            case "0":
                $cmd = "<always>";
                break;
            case "1":
                $cmd = "<auto>";
                break;
            case "2":
                $cmd = "<never>";
                break;
            default:
                return false;
        }
        $result = $this->send("<set_network_mode>\n".$cmd."\n</set_network_mode>\n");
        if (!$result) return false;

        if (($buf = $this->$read()) !== false) {
            return (preg_match("<success/>", $buf));
        }
    }

    function get_messages($seqno) {

        if (!$this->socket) return false;

        if (!$this->authorize()) return false;

        $result = $this->send("<get_messages>\n<seqno>".$seqno."</seqno>\n</get_messages>\n");
        if (!$result) return false;

        // receive data
        $data = $this->read();

        // parse data
        $msgs = array();
        $i = -1;
        $strings = explode("\n", $data);
         while ($buf = $strings[++$i]) {
            if (match_tag($buf, "<msgs>")) continue;
            if (match_tag($buf, "</msgs>")) break;
            if (match_tag($buf, "<msg>")) {
                $md = array("project"=>"---");
                while ($buf = $strings[++$i]) {
                    if (match_tag($buf, "</msg>")) break;
                    if (parse_string($buf, "project", $md["project"])) continue;
                    if (match_tag($buf, "<body>")) {
                        $md["body"] = "";
                        while ($buf = $strings[++$i]) {
                            if (match_tag($buf, "</body>")) break;
                            $md["body"] .= $buf;
                        }
                        continue;
                    }
                    if (parse_int($buf, "pri", $md["priority"])) continue;
                    if (parse_int($buf, "time", $md["timestamp"])) continue;
                    if (parse_int($buf, "seqno", $md["seqno"])) continue;
                }
                array_push($msgs, $md);
            }
        }
        $result = "<div id=\"divmessages\">
            <table class=\"ui celled table\">
            <thead>
                <tr><th width=\"20%\">Project</th>
                <th width=\"20%\">Date</th>
                <th>Body</th>

            </tr></thead>
            <tbody>";

         $index = count($msgs);   
         while ($index) {
            $index -= 1;
            $result .= "<tr><td>" . $msgs[$index]["project"] . "</td>";
            $result .= "<td>" . date(DATE,$msgs[$index]["timestamp"]) . "</td>";
            $result .= "<td>" . $msgs[$index]["body"] . "</td></tr>";
         }   
         $result .= "</tbody><tfoot></tfoot></table></div>";   
        return $result;
        
    }

    function authorize() {
   
        $password = retrieveGUIPassword();

        if ($password == false ) {
            return false;
        }

        $cmd =  "<auth1/>";
        $result = $this->send($cmd);
        if (!$result) return false;
        if (($buf = $this->read()) === false) {
            return false;
        }

        $xml = simplexml_load_string( rtrim($buf, "\003" ));
        $nonce = (string)$xml->nonce;

        $cmd =  "<auth2>\n".
                "<nonce_hash>".md5($nonce.$password)."</nonce_hash>\n".
                "</auth2>";
        $result = $this->send($cmd);
        if (!$result) return false;
        if (($buf = $this->read()) === false) {
            return false;
        }

        if (strpos($buf, "unauthorized") == 0) {
            return true;
        }
        return false;
    }

}

?>
