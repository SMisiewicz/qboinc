<?php

include_once("gui_rpc_client.php");
define("HOST", "localhost");
define("ADDRESS", 31416);

function renderPage($authorized) {

    $client = new RPC_CLIENT();
    if (! $client->connect(HOST, ADDRESS)) {
        echo "<body><h1>offline</h1></body>";
        return;
    }  

    $page = "";
    $result = ""; $project = "";
    $state = $client->get_state();
    parse_state($state, $authorized, $result, $hardware, $project);

    $page .= menu($authorized);

    if ($authorized) {
        $page .= $hardware;
        $page .= options();
        $page .= $project;
    }
    $page .= $result;
    if ($authorized) {
        $page .= $client->get_messages(0);
    }
    $page .= "</div>";

    $client->close();
    return $page;
}

function createAccount($master_url, $email, $passwd_hash) {
    $r = "{status: error}";
    $client = new RPC_CLIENT();
    if ($client->connect(HOST, ADDRESS)) {
        if ($client->lookup_account($master_url, $email, $passwd_hash, $result)) {
            //key or wrong password (-206)
            $r = "{status: error, message: \"Exists\"}";
        } else {
            if ($result == "-136") { //does not exists
                if ($client->create_account($master_url, $email, $passwd_hash, $result)) {
                    if ($client->project_attach($master_url, $result)) {
                        $r = "{status: ok}";
                    }
                }
            } else {
                $r = "{status: error, message: \"Received ".$result."\"}"; 
            }
        }
    }
    $client->close(); 
    return $r; 
}

function projectAction($master_url, $url_query, $key) {
    $r = "{status: error}";
    $client = new RPC_CLIENT();
    if ($client->connect(HOST, ADDRESS)) {
        if ($url_query == "pause") {
            if($client->project_suspend($master_url)) {
                $r =  "{status: ok}";
            } 
        } else if ($url_query == "play") {
            if($client->project_resume($master_url)) {
                $r = "{status: ok}";
            }
        } else if ($url_query == "detach") {
            if($client->project_detach($master_url)) {
                $r = "{status: ok}";
            }  
        } else if ($url_query == "attach") {
            if($client->project_attach($master_url, $key)) {
                $r = "{status: ok}";
            }  
        } else if ($url_query == "freeze") {
            if($client->project_freeze($master_url, $key)) {
                $r =  "{status: ok}";
            } 
        } else if ($url_query == "thaw") {
            if($client->project_thaw($master_url, $key)) {
                $r = "{status: ok}";
            }  
        }
    }
    $client->close(); 
    return $r; 
}

function resultAction($master_url, $result, $url_query) {
    $r = "{status: error}";
    $client = new RPC_CLIENT();
    if ($client->connect(HOST, ADDRESS)) {
        if ($url_query == "pause") {
            if($client->result_suspend($result, $master_url)) {
                $r = "{status: ok}";
            }  
        } else if ($url_query == "play") {
            if($client->result_resume($result, $master_url)) {
                $r = "{status: ok}";
            }  
        } else if ($url_query == "abort") {
            if($client->result_abort($result, $master_url)) {
                $r = "{status: ok}";
            } 
        }
    }
    $client->close();  
    return $r;  
}

function menu($authorized) {

    $opt = "";
    if ($authorized) {
        $opt ="<button class=\"ui compact icon button\" onclick=\"logout();\">Logout</button>";
    } else {
        $opt = "<div class=\"ui modal login\" style=\"width:600px;\">
        <i class=\"close icon\"></i>
        <div class=\"header\">Please enter your password</div>
        <div class=\"content\">
            <div class=\"ui form\">
                <div style=\"width:300px;\" class=\"field\"><input id=\"pass\" type=\"password\"></div>
            </div>
        </div>
        <div class=\"actions\">
        <div class=\"ui button\" onclick=\"$('.modal.login').modal('hide');\">Cancel</div>
        <div class=\"ui green button\" onclick=\"login();\">OK</div>
        </div>
        </div><button class=\"ui compact icon button\" onclick=\"$('.modal.login').modal('show');\">Login</button>"; 
    }
    return "<div style=\"float:right;margin:10px\">". $opt ."</div>";
}

function options() {
    return  "<div class=\"ui buttons\">"
        ."<button class=\"ui button bprojects\" data-vivaldi-spatnav-clickable=\"1\">Projects</button>"
        ."<button class=\"ui button bresults\" data-vivaldi-spatnav-clickable=\"1\">Results</button>"
        ."<button class=\"ui button bmessages\" data-vivaldi-spatnav-clickable=\"1\">Messages</button>"
        ."</div><hr>";
}

function parse_state($state, $authorized, &$resultTable, &$hardware, &$project) {

    date_default_timezone_set("UTC");

    $xml=simplexml_load_string( rtrim($state, "\003" ));
    $client_state = $xml->client_state;
    $result = "<div id=\"divresults\">
        <table class=\"ui celled table\">
        <thead>
            <tr><th>Name</th>
            <th>Received date</th>
            <th>Report dedline</th>
            <th>% done</th>
            <th>Elapsed</th>
            <th>Remaining</th>";
    if ($authorized) {
        $result .= "<th>Action</th>";        
    }    
    $result .= "</tr></thead><tbody>";
    foreach($client_state->result as $r) {
        $result .= "<tr>"
            ."<td>".$r->name."</td>"
            ."<td>".date(DATE, (float)$r->received_time)."</td>"
            ."<td>".date(DATE, (float)$r->report_deadline)."</td>";
        
        if (isset($r->active_task)) {
            if (isset($r->project_suspended_via_gui) || isset($r->suspended_via_gui)) {
                $result .= "<td style=\"background-color:#e4e814;\">".number_format((float)$r->active_task->fraction_done*100,2)."</td>";
            } else {
                $result .= "<td style=\"background-color:#38c13f;\">".number_format((float)$r->active_task->fraction_done*100,2)."</td>";                        
            }
            $result .= "<td>".date("H:i:s", (float)$r->active_task->elapsed_time)."</td>";
            $result .= "<td>".date("H:i:s", (float)$r->estimated_cpu_time_remaining)."</td>";
        } else {
            $result .= "<td style=\"background-color:#c13838\";>NOT ACTIVE</td><td></td><td></td>";
        }
        

        
        if ($authorized) {
            if (isset($r->active_task)) { //suspend/resume result
                $project_url = $r->project_url;
                $result .= "<td>";
                if (!isset($r->project_suspended_via_gui)) { //suspended via gui
                    if (isset($r->suspended_via_gui)) {
                        $result .= "<button class=\"ui compact icon button\" onclick=\"result('play', '" . $project_url . "', '" . $r->name . "');\"><i class=\"play icon\"></i></button>";
                    } else {
                        $result .= "<button class=\"ui compact icon button\" onclick=\"result('pause', '" . $project_url . "', '" . $r->name . "');\"><i class=\"pause icon\"></i></button>";
                    }
                    $result .= "<button class=\"ui compact icon button\" onclick=\"result('abort', '" . $project_url . "', '" . $r->name . "');\"><i class=\"trash icon\"></i></button>";
                }
                $result .="</td>";
            } else {
                $result .= "<td></td>";
            }
        }
        $result .= "</tr>";
    }
    $result .= "</tbody><tfoot></tfoot></table></div>";
    $resultTable = $result;

    $host_info = $client_state->host_info;
    $result = "<h3 class=\"ui header\"><i class=\"plug icon\"></i>".
    "<div class=\"content\"><div>". $host_info->domain_name . " " . $host_info->os_name . " " . $host_info->os_version ."</div>".
    "<div class=\"sub header\"><div></div><div>". $host_info->p_model ."</div></div></div></h3>";
    $result .= "<div>Current time :".date("H:i:s")." (".date_default_timezone_get().")</div><br>";
    $hardware = $result;

    $result = "<div id=\"divprojects\">
        <table class=\"ui celled table\" id=\"projecttable\">
        <thead>
            <tr><th>Project name</th>
            <th>Project url</th>
            <th>User name</th>
            <th>Total credit</th>
            <th>Options</th>
            <th>Info</th>   
        </tr></thead>
        <tbody>";

    foreach($client_state->project as $project_info) {

        $project_options = "";
        $suspended_via_gui = false;
        $dont_request_more_work = false;
        if (isset($project_info->suspended_via_gui)) {
            $project_options .= "Suspended via gui<br>";
            $suspended_via_gui = true;
        }
        if (isset($project_info->dont_request_more_work)) {
            $project_options .= "Dont' request more work<br>";
            $dont_request_more_work = true;
        }
        if (isset($project_info->detach_when_done)) {
            $project_options .= "Detach when done<br>";
        }       
        $master_url = $project_info->master_url;

        if ( $master_url != NULL ) {    
            $result .= "<tr><td>" . $project_info->project_name ."</td>"
            ."<td>" . $master_url ."</td>"
            ."<td>" . $project_info->user_name ."</td>"
            ."<td>" . $project_info->user_total_credit ."</td>"
            ."<td>";
            if ($suspended_via_gui) {
                $result .= "<button class=\"ui compact icon button\" onclick=\"project('play', '" . $master_url . "');\"><i class=\"play icon\"></i></button>";
            } else {
                $result .= "<button class=\"ui compact icon button\" onclick=\"project('pause', '" . $master_url .  "');\"><i class=\"pause icon\"></i></button>";
            }
            if ($dont_request_more_work) {
                $result .= "<button class=\"ui compact icon button\" onclick=\"project('thaw', '" . $master_url .  "');\"><i class=\"video play icon\"></i></button>";
            } else {
                $result .= "<button class=\"ui compact icon button\" onclick=\"project('freeze', '" . $master_url .  "');\"><i class=\"pause circle icon\"></i></button>";
            }
            $result .= "<button class=\"ui compact icon button\" onclick=\"project('detach', '" . $master_url .  "');\"><i class=\"trash icon\"></i></button>";
            $result .= "</td><td>" . $project_options ."</td></tr>";
        } 
    }

    $result .= "</tbody><tfoot></tfoot></table>";
    if ($authorized) {
        $result .= attachProjectDialog();
    }
    
    $project = $result."</div>";

}

function attachProjectDialog() {
    $r = "<button class=\"ui compact icon button\" onclick=\"showAttachDialog();\"><i class=\"add user icon\"></i></button>";
    $r .= "<div class=\"remodal\" data-remodal-id=\"modal\">
  <button data-remodal-action=\"close\" class=\"remodal-close\"></button>
    <h4>Please enter</h4>
    <h5>project Url and Key</h5>
        <div class=\"ui form\">
            <div class=\"field\"><input id=\"attachProjectUrl1\"></div>            
            <div class=\"field\"><input id=\"attachProjectKey\"></div>
        </div>
      <div class=\"ui horizontal divider\">or</div>
	<h5>project Url, email and secret</h5>
        <div class=\"ui form\">
            <div class=\"field\"><input id=\"attachProjectUrl2\"></div>            
            <div class=\"field\"><input id=\"attachProjectEmail\"></div>
            <div class=\"field\"><input id=\"attachProjectSecret\"></div> 
        </div>       
   <br>     
  <div style=\"margin:30px;\"></div>  
  <button data-remodal-action=\"cancel\" class=\"remodal-cancel\">Cancel</button>
  <button data-remodal-action=\"confirm\" class=\"remodal-confirm\">OK</button>
</div>";
    return $r;
}

?>
