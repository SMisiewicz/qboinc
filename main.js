var interval;
function autorefresh() {
    var cbrefresh = $("#cbrefresh:checkbox:checked").val();
    if ( cbrefresh == "on" && interval === undefined ) {
       interval = setInterval(function(){
            started = true
            if ($('#divrefresh').is(':visible')) {
                reload(1)
            }
        }, 5000);
    } else {
        if ( interval !== undefined) {
            clearInterval(interval)
            interval = undefined
        } 
    }
}

function logout() {
    $.post( "api.php", {action: "logout" }, function( data ) {
        var response = JSON.parse(data)
        if (response.success === true) {
            reload(1);
        }
    })
}

function login() {
    var hash = md5( $("#pass").val() , 'qboinc');
    $.post( "api.php", { action: "login", pass: hash } , function( data ) {
        var response = JSON.parse(data);
        if (response.success === true ) {
            $('.modal').modal('hide')
            reload(0)
        } else {
            alert('Wrong password')
        }
    });
}

function project(action, url) {
    if (action == 'detach') {
        //???????/
    }
    $.post( "api.php", { action: action, master_url: url } , function( data ) {
        reload(0);
    });
}

function result(action, url, name) {
    $.post( "api.php", { action: action, master_url: url, result: name } , function( data ) {
        reload(1);
    });
}

function projectAttach(url, parm1, parm2) {
    if (parm2 === undefined) {
        $.post( "api.php", { action: "attach", master_url: url, key: parm1 } , function( data ) {
            reload(0);
        });
    } else {
        $.post( "api.php", { action: "createaccount", master_url: url, email: parm1, passwd_hash: parm2 } , function( data ) {
            reload(0);
        });        
    }
}

function reload(id) {
    $.get( "api.php?loadpage", function( data ) {
        $("#page").html(data)
        attachActions(id)
    })    
}

function attachActions(id) {
    $(".bprojects").click( function()  {
        showdivs(0);
    })
    $(".bresults").click( function()  {
        showdivs(1);
    })  
    $(".bmessages").click( function()  {
        showdivs(2);
    })  
    showdivs(id); 
}

function showAttachDialog() {
    var inst = $('[data-remodal-id=modal]').remodal();
    inst.open();
}

function showdivs(id) {
    $("#divrefresh").hide()
    $("#divprojects").hide()
    $("#divresults").hide()
    $("#divmessages").hide()    
    if (id == 0) {
        $("#divprojects").show()
    } else if (id == 1) {
        $("#divresults").show()
        $("#divrefresh").show()
    } else if (id == 2) {
        $("#divmessages").show()
    }
}

$( document ).ready(function() {
    
    $(document).on('confirmation', '.remodal', function () {
        var url1 = $('#attachProjectUrl1').val().trim()
        var key = $('#attachProjectKey').val().trim()
        var url2 = $('#attachProjectUrl2').val().trim()
        var email = $('#attachProjectEmail').val().trim()
        var secret = $('#attachProjectSecret').val().trim()
        if  ( url1 == '' && key == '' &&
              url2 == '' && email == '' && secret == '' ) {
            alert('Please provide valid parameters')
            return;
        }        
        if (url1 != '' && key != '') {
            projectAttach(url1, key)
        } else {
            var split = email.split("@");
            if ( split.length != 2 ) {
                alert("Incorrect email");
                return;
            }
            projectAttach(url2, email, md5(secret + split[0].toLowerCase()))
        }
    });

    reload(1);
});