#!/bin/sh

BOINC=`command -v boinc||echo NOT_FOUND`
if [ "$BOINC" = "NOT_FOUND" ]; then
	echo "BOINC not found in PATH, checking current dir"
	if [ ! -x ./boinc ]; then
		echo "BOINC executable not found"
		exit 1;
	fi
	BOINC="./boinc"
fi


echo "checking current if data_dir exists"
if [ ! -d ./data_dir ]; then
	echo "creating data_dir"
	mkdir ./data_dir || exit 1
fi

if [ ! -f ./stamp ]; then
	echo "creating stamp"
	echo -n "1481805547" > stamp
fi

if [ ! -f ./data_dir/gui_rpc_auth.cfg ]; then
	echo "creating gui_rpc_auth.cfg"
	echo -n "09bb49b6bc7a1e006245bd99b34b3196" > ./data_dir/gui_rpc_auth.cfg
fi

echo "starting boinc"
$BOINC -dir ./data_dir --daemon
