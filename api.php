<?php

include_once("boinc_gui.php");
define("LOCALPASS", "0f4e01a2e5e3b99660e3441e7fdcc724");

$url = $_SERVER['REQUEST_URI'];
$url_query =  parse_url($url, PHP_URL_QUERY);
$url_path =  parse_url($url, PHP_URL_PATH);

if (isset($_GET["loadpage"])) {
    $authorized = false;
    if (isset($_COOKIE["qboincpass"])) {
        $pass = $_COOKIE["qboincpass"];
        if ($pass == LOCALPASS ) {
            $authorized = true;
        }
    }
    echo renderPage($authorized);
    return;
}

//create account
if (isset($_POST["action"]) && $_POST["action"] == "createaccount") {

    if (!isLogged()) return;
    $master_url = $_POST["master_url"];
    $email = $_POST["email"];
    $passwd_hash = $_POST["passwd_hash"];
    echo createAccount($master_url, $email, $passwd_hash);
    return;
}

//suspend/resume/abort result
if (isset($_POST["master_url"]) && isset($_POST["result"])) {
    
    if (!isLogged()) return;
    
    $master_url = $_POST["master_url"];
    $result = $_POST["result"];    
    $action = $_POST["action"];
    echo resultAction( $master_url, $result, $action);
    return;
}

//suspend/resume/detach/attach/freeze/thaw project
if (isset($_POST["master_url"])) {
    
    if (!isLogged()) return;
    
    $master_url = $_POST["master_url"];
    $action = $_POST["action"];
    $key = "";
    if ($action == "attach" && isset($_POST["key"])) {
        $key = $_POST["key"];
    }
    echo projectAction( $master_url, $action, $key);
    return;
}

if (isset($_POST["action"]) && $_POST["action"] == "login") {
    $pass = $_POST["pass"];
    $r = "{\"success\": false}";
    if ($pass == LOCALPASS) {
        setcookie("qboincpass", $pass, time() + 60 * 15); 
        $r = "{\"success\": true}";
    }
    echo($r);
    return;
}

if (isset($_POST["action"]) && $_POST["action"] == "logout") {
    setcookie("qboincpass", "", time() - 3600); 
    echo("{\"success\": true}");
    return;
}

function isLogged() {
    if ( !isset($_COOKIE["qboincpass"]) ) {
        echo "{\"success\": false, \"message\": \"unauthorized\"}";
        return false;    
    }
    $pass = $_COOKIE["qboincpass"];
    if ($pass != LOCALPASS ) {
        echo "{\"success\": false, \"message\": \"unauthorized\"}";    
        return false;
    }
    return true;
}

echo("{\"success\": false, \"message\": \"unknown request\"}");

?>
